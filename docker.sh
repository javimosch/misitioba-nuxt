docker run -d --rm --name misitioba-nuxt \
--env-file ./.env -w /app \
-v "$(pwd)/build:/app/build"  \
-v "$(pwd)/assets/:/app/assets"  \
-v "$(pwd)/layouts:/app/layouts"  \
-v "$(pwd)/components:/app/components"  \
-v "$(pwd)/middleware:/app/middleware"  \
-v "$(pwd)/pages:/app/pages"  \
-v "$(pwd)/plugins:/app/plugins"  \
-v "$(pwd)/static:/app/static"  \
-v "$(pwd)/store:/app/store"  \
-v "$(pwd)/.editorconfig:/app/.editorconfig"  \
-v "$(pwd)/.env:/app/.env"  \
-v "$(pwd)/jsonconfig.json:/app/jsonconfig.json"  \
-v "$(pwd)/nuxt.config.js:/app/nuxt.config.js"  \
-v "$(pwd)/package.json:/app/package.json"  \
-v "$(pwd)/package-lock.json/:/app/package-lock.json"  \
-v "$(pwd)/docker-entry.sh:/app/docker-entry.sh"  \
-v "/root/.npm/_cacache:/root/.npm/_cacache"  \
node:13.5.0-alpine sh docker-entry.sh